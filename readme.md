**TEMARIO INSTRUCCIONES**

**Carpeta con landing base**

- Archivo html
	- Head debe contener:
		- cdn bootstrap
		- link a estilo css propio (en carpeta css)
		- cdn google fonts
		- cdn font awesome
		- favicon
		- google analytics
		- title (título página)
	- Body debe contener:
		- contenido de la landing
		- justo antes del cierre del body (para que funcione todo correctamente), los scripts de:
			- bootstrap
			- botón “subir”
			- botón “read more” (si es necesario).
- Carpeta css con archivo css
- Carpeta images con imágenes dentro

<hr>

**TEMA BASE INCLUYE**

- Header
- Jumbotron
- Title
- Text
- Video
- Button
- Button with font awesome icon
- Cards with icon
- Image Slider
- Video Slider
- Single Image
- Accordeon with info
- Cards with Read More
- Button “volver arriba” with font awesome icon
- Hashtag 
- Footer

<hr>

[Instrucciones Subida de Campaña a **Gob.cl**](https://gitlab.com/triniswinburn/tema-base-campana-gob/blob/master/intrucciones-campana.pdf)

<hr>

[**Previsualización** de tema base](https://stupefied-bell-459708.netlify.com/)

<hr>

**RECURSOS**

**1. [Documentación de Bootstrap 4.1](https://getbootstrap.com/docs/4.1/getting-started/introduction/)**

Componentes, recursos, clases, instrucciones y documentación general de Bootstrap 4.1.

**CSS que va en head:**

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

**SCRIPTS que van justo antes de cerrar el body:**

	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<hr>

**2. [Google fonts](https://fonts.google.com/)**

Amplia librería de fuentes tipográficas, sus instrucciones de agregado en head de html y en estilo css.

**Ejemplo de cómo insertar la font en el head:**

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
		
Ejemplo de como agregar la regla css a una clase:

	font-family: 'Source Sans Pro', sans-serif;

<hr>

**3. [Font Awesome](https://fontawesome.com/icons)**

Librería de íconos para diversos usos, sus intrucciones de agregado de CDN en head de html y en el body del html. A través de las clases se pueden estilizar individual o grupalmente.

**CDN que va en el head:**

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	 
**Ejemplo de qué debe insertarse en el html cuando se quiera agregar un ícono de Font Awesome:**

	<i class="fas fa-angle-double-right"></i>

<hr>

**4. [Codepen](https://codepen.io/)**

En el caso de necesitar alguna funcionalidad o elemento diferente, puedes utilizar el buscador de codepen y buscar el recurso realizado por alguien más y adaptarlo a la necesidad.

<hr>

**5. [Favicon](https://cdn.digital.gob.cl/favicon/favicon-32x32.png)**

Todas las campañas o landings de gobierno deben tener favicon (ícono de pestaña web abierta).

**Código que debe ir en el head:**

	<!----------------- FAVICON ----------------->
    <link rel="apple-touch-icon" sizes="57x57" href="https://cdn.digital.gob.cl/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://cdn.digital.gob.cl/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://cdn.digital.gob.cl/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://cdn.digital.gob.cl/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://cdn.digital.gob.cl/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://cdn.digital.gob.cl/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://cdn.digital.gob.cl/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://cdn.digital.gob.cl/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://cdn.digital.gob.cl/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="https://cdn.digital.gob.cl/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://cdn.digital.gob.cl/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="https://cdn.digital.gob.cl/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://cdn.digital.gob.cl/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://cdn.digital.gob.cl/favicon/manifest.json">

<hr>
  
**6. Google Analytics**

**Hay que agregar el siguiente código en el head:**

	<!----------------- GOOGLE ANALYTICS ----------------->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-48790041-1"></script>
    <script>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-48790041-1');</script>

<hr>
  
**7. En caso de tener un fixed-nav agregar este css:**

	.cms-toolbar-expanded body, .cms-toolbar-expanded .navbar-fixed-top{
    	top:46px;
	}
	
Esto, evitará que la barra de Django se superponga al contenido (header) del landing.

<hr>

***FIN***